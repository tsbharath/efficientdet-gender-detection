#Step 1 

1. Clone to https://github.com/EscVM/OIDv4_ToolKit to get open images data
2. To get train data for Man and Woman classes :  python3 main.py downloader -y --classes Man Woman --type_csv train --image_IsGroupOf 0 --limit 10000
3. To get validation data for Man and Woman classes :  python3 main.py downloader -y --classes Man Woman --type_csv validation --image_IsGroupOf 0 --limit 2000


#Step 2: 

1. Clone to https://github.com/zylo117/Yet-Another-EfficientDet-Pytorch
2. Prepare data as per cocodataset requirement using notebook "EfficientDet - data prep.ipynb "
3. Train a new model with pre-trained weights: python train.py -c 2 -p oid --batch_size 8 --lr 1e-3 --num_epochs 1  -w '/Yet-Another-EfficientDet-Pytorch/weights/efficientdet-d7.pth'  --head_only True --debug True



